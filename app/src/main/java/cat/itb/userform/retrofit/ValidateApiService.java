package cat.itb.userform.retrofit;

import cat.itb.userform.models.ValidateModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ValidateApiService {

    @GET("{user}/login.json")
    Call<ValidateModel> getLoginAuth(@Path("user") String user);

    @GET("{user}/register.json")
    Call<ValidateModel> getRegisterAuth(@Path("user") String user);
}
