package cat.itb.userform;

import android.view.View;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isNotChecked;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testLogin() {
        onView(withId(R.id.wellcomeLoginButton)).perform(click());
        onView(withId(R.id.loginUsernameEdit)).perform(replaceText("user1"));
        onView(withId(R.id.loginPasswordEdit)).perform(replaceText("hola"));
        onView(withId(R.id.loginLoginButton)).perform(click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.helloTester)).check(matches(isDisplayed()));

    }

    @Test
    public void testErrorInLogin() {
        onView(withId(R.id.wellcomeLoginButton)).perform(click());
        onView(withId(R.id.loginLoginButton)).perform(click());
        onView(withId(R.id.loginUsername)).check(matches(hasTextInputLayoutErrorText("Required Field")));
        onView(withId(R.id.loginPassword)).check(matches(hasTextInputLayoutErrorText("Required Field")));
    }

    @Test
    public void testRegisterCorrect() {
        onView(withId(R.id.wellcomeRegisterButton)).perform(click());
        onView(withId(R.id.registerUsernameEdit)).perform(replaceText("user2"));
        onView(withId(R.id.registerPasswordEdit)).perform(replaceText("hola"));
        onView(withId(R.id.registerRepeatPasswordEdit)).perform(replaceText("hola"));
        onView(withId(R.id.registerEmailEdit)).perform(replaceText("hola"));
        onView(withId(R.id.checkbox)).check(matches(isNotChecked())).perform(scrollTo(), click());
        onView(withId(R.id.registerRegisterButton)).perform(scrollTo(), click());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.helloTester)).check(matches(isDisplayed()));
    }

    @Test
    public void testRegisterErrorsDisplayed() {
        onView(withId(R.id.wellcomeRegisterButton)).perform(click());
        onView(withId(R.id.registerRegisterButton)).perform(scrollTo(), click());
        onView(withId(R.id.registerUsername)).perform(scrollTo()).check(matches(hasTextInputLayoutErrorText("Required Field")));
        onView(withId(R.id.registerPassword)).perform(scrollTo()).check(matches(hasTextInputLayoutErrorText("Required Field")));
        onView(withId(R.id.registerRepeatPassword)).perform(scrollTo()).check(matches(hasTextInputLayoutErrorText("Required Field")));
        onView(withId(R.id.registerEmail)).perform(scrollTo()).check(matches(hasTextInputLayoutErrorText("Required Field")));
    }

    @Test
    public void testNavLoginRegister(){
        onView(withId(R.id.wellcomeRegisterButton)).perform(click());
        onView(withId(R.id.registerUsername)).check(matches(isDisplayed()));
        onView(withId(R.id.registerLoginButton)).perform(scrollTo(), click());
        onView(withId(R.id.loginUsername)).check(matches(isDisplayed()));
        onView(withId(R.id.loginRegisterButton)).perform(click());
        onView(withId(R.id.registerUsername)).check(matches(isDisplayed()));

    }

    public static Matcher<View> hasTextInputLayoutErrorText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) view).getError();

                if (error == null) {
                    return false;
                }

                String errorText = error.toString();

                return expectedErrorText.equals(errorText);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }
}

