package cat.itb.userform.ui.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.models.ValidateModel;

public class LoginFragment extends Fragment {

    @BindView(R.id.loginUsername)
    TextInputLayout loginUsername;
    @BindView(R.id.loginPassword)
    TextInputLayout loginPassword;
    private LoginViewModel mViewModel;
    ProgressDialog dialog;


    private static final String AUTHCODE = "a8a462fc-fc51-45a5-aa59-38c219dae62e";


    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mViewModel.getLoading().observe(getViewLifecycleOwner(), this::onLoadingFinish);
        mViewModel.getLogged().observe(getViewLifecycleOwner(), this::onLoginTrue);
        mViewModel.getError().observe(getViewLifecycleOwner(), this::onLoginFailed);
    }

    @OnClick({R.id.loginLoginButton, R.id.loginRegisterButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginLoginButton:
                if (validate()) {
                    CheckApi();
                }
                break;
            case R.id.loginRegisterButton:
                Navigation.findNavController(view).navigate(R.id.goToRegisterFromLogin);
                break;
        }
    }

    private void CheckApi() {
        mViewModel.loginAuth(loginUsername.getEditText().getText().toString());

    }

    private void onLoadingFinish(Boolean aBoolean) {
        if (aBoolean){
            dialog = ProgressDialog.show(getContext(), "Loading", "loading", true);
            dialog.show();
        }else {
            if(dialog!=null){
                dialog.dismiss();
                dialog = null;
            }
        }

    }

    private void onLoginFailed(String s) {
        Snackbar snackbar = Snackbar.make(getView(), "User not found or user or password incorrect", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void onLoginTrue(Boolean aBoolean) {
        if (aBoolean) {
            Navigation.findNavController(getView()).navigate(R.id.goToLoggedFromLogin);
        }
    }

    private boolean validate() {
        boolean validate = true;
        loginUsername.setError("");
        loginPassword.setError("");
        validate = validateNotEmpty(loginUsername, validate);
        validate = validateNotEmpty(loginPassword, validate);
        return validate;
    }

    private boolean validateNotEmpty(TextInputLayout layout, boolean validate) {
        if (layout.getEditText().getText().toString().isEmpty()) {
            layout.setError("Required Field");
            scrollTo(layout);
            validate = false;
        }
        return validate;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }
}
