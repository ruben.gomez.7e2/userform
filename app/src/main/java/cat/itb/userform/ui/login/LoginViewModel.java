package cat.itb.userform.ui.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.userform.models.ValidateModel;
import cat.itb.userform.retrofit.ValidateApiService;
import cat.itb.userform.retrofit.ValidateRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginViewModel extends ViewModel {

    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<>();

    public LiveData<Boolean> getLogged() {
        return logged;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<String> getError() {
        return error;
    }


    private static final String BASE_URL = "http://itb.mateuyabar.com/DAM-M07/UF1/exercicis/userform/fakeapi/";

    private ValidateApiService validateApiService;

    public LoginViewModel() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        validateApiService = retrofit.create(ValidateApiService.class);
    }


    public void loginAuth(String user) {
        loading.setValue(true);
        Call<ValidateModel> call = validateApiService.getLoginAuth(user);
        call.enqueue(new Callback<ValidateModel>() {
            @Override
            public void onResponse(Call<ValidateModel> call, Response<ValidateModel> response) {
                if (response.body().getAuthToken() != null) {
                    logged.postValue(true);
                } else error.postValue(response.body().getError());
                loading.postValue(false);
            }

            @Override
            public void onFailure(Call<ValidateModel> call, Throwable t) {
                error.postValue(t.getLocalizedMessage());
                loading.postValue(false);
            }
        });


    }
}
