package cat.itb.userform.ui.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.userform.models.ValidateModel;
import cat.itb.userform.retrofit.ValidateApiService;
import cat.itb.userform.retrofit.ValidateRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterViewModel extends ViewModel {

    public LiveData<ValidateModel> RegisterAuth(String user){
        return getRegisterAuth(user);
    }


    private static final String BASE_URL = "http://itb.mateuyabar.com/DAM-M07/UF1/exercicis/userform/fakeapi/";

    private ValidateApiService validateApiService;

    public RegisterViewModel() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        validateApiService = retrofit.create(ValidateApiService.class);
    }


    public LiveData<ValidateModel> getRegisterAuth(String user){
        MutableLiveData<ValidateModel> mutableValidate = new MutableLiveData<>();

        Call<ValidateModel> call = validateApiService.getRegisterAuth(user);
        call.enqueue(new Callback<ValidateModel>() {
            @Override
            public void onResponse(Call<ValidateModel> call, Response<ValidateModel> response) {
                mutableValidate.postValue(response.body());
            }

            @Override
            public void onFailure(Call<ValidateModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return mutableValidate;
    }
}
