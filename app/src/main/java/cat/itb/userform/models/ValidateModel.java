package cat.itb.userform.models;

import com.google.gson.annotations.SerializedName;

public class ValidateModel {

    //@SerializedName("authToken")
    String authToken;
    //@SerializedName("error")
    String error;

    public ValidateModel() {
    }

    public ValidateModel(String authToken, String error) {
        this.authToken = authToken;
        this.error = error;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
