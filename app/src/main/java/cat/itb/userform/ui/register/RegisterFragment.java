package cat.itb.userform.ui.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.models.ValidateModel;

public class RegisterFragment extends Fragment {

    @BindView(R.id.registerUsername)
    TextInputLayout registerUsername;
    @BindView(R.id.registerPassword)
    TextInputLayout registerPassword;
    @BindView(R.id.registerRepeatPassword)
    TextInputLayout registerRepeatPassword;
    @BindView(R.id.registerEmail)
    TextInputLayout registerEmail;
    @BindView(R.id.registerName)
    TextInputLayout registerName;
    @BindView(R.id.registerSurnames)
    TextInputLayout registerSurnames;
    @BindView(R.id.registerBirthDate)
    EditText registerBirthDate;
    @BindView(R.id.genderRegister)
    EditText genderRegister;
    @BindView(R.id.checkbox)
    MaterialCheckBox checkbox;

    private static final String AUTHCODE = "a8a462fc-fc51-45a5-aa59-38c219dae62e";

    String[] optionsArray = {"Hombre", "Mujer", "Neutro", "No quiero decirlo"};

    private RegisterViewModel mViewModel;


    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        // TODO: Use the ViewModel
    }

    @OnClick({R.id.registerRegisterButton, R.id.registerLoginButton, R.id.registerBirthDate, R.id.genderRegister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.registerRegisterButton:
                register();
                break;
            case R.id.registerLoginButton:
                Navigation.findNavController(view).navigate(R.id.goToLoginFromRegister);
                break;
            case R.id.registerBirthDate:
                onRegisterBirthDateClicked();
                break;
            case R.id.genderRegister:
                onGenderRegisterClicked();
                break;
        }
    }

    private void register() {
        if(validate()){
            CheckApi();
        }
    }

    private boolean validate() {
        boolean validate = true;
        registerUsername.setError("");
        registerPassword.setError("");
        registerRepeatPassword.setError("");
        registerEmail.setError("");
        validate = validateNotEmpty(registerUsername, validate);
        validate = validateNotEmpty(registerPassword, validate);
        validate = validateNotEmpty(registerRepeatPassword, validate);
        validate = validateNotEmpty(registerEmail, validate);

        if (!registerPassword.getEditText().getText().toString().equals(registerRepeatPassword.getEditText().getText().toString())) {
            Toast.makeText(getContext(), "Las contraseñas no son iguales", Toast.LENGTH_LONG).show();
            validate = false;
        }
        if (!checkbox.isChecked()) {
            validate = false;
            Toast.makeText(getContext(), "Has de aceptar los terminos y condiciones", Toast.LENGTH_LONG).show();
        }
        return validate;

    }

    private void CheckApi() {
        LiveData<ValidateModel> apiAuthCode = mViewModel.getRegisterAuth(registerUsername.getEditText().getText().toString());
        apiAuthCode.observe(this, this::onChange);
    }

    private void onChange(ValidateModel validateModel) {
        if (validateModel.getAuthToken()!=null) {

            Navigation.findNavController(getView()).navigate(R.id.goToLoggedFromRegister);

        } else {
            Snackbar snackbar = Snackbar.make(getView(), "User in use", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }


    private boolean validateNotEmpty(TextInputLayout layout, boolean validate) {
        if (layout.getEditText().getText().toString().isEmpty()) {
            layout.setError("Required Field");
            scrollTo(layout);
            validate = false;
        }
        return validate;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

    public void onRegisterBirthDateClicked() {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("Birth Date");
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());
    }

    private void doOnDateSelected(Long aLong) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        registerBirthDate.setText(dateFormat.format(new Date(aLong)));
    }

    public void onGenderRegisterClicked() {
        new MaterialAlertDialogBuilder(getContext())
                .setTitle("Genero")
                .setItems(optionsArray, this::onGenderSelected)
                .show();
    }

    private void onGenderSelected(DialogInterface dialogInterface, int i) {
        genderRegister.setText(optionsArray[i]);
    }

}
